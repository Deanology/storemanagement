﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StoreManagementSystem.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required, StringLength(100), Display(Name= "Name")]
        public string ProductName { get; set; }
        [Required, StringLength(1000), Display(Name ="Product Description"), DataType(DataType.MultilineText)]
        public string ProductDescription { get; set; }
        [Required, Display(Name = "Product Image")]
        public string ProductImagePath { get; set; }
        [Required, Display(Name = "Price")]
        public double ProductUnitPrice { get; set; }
        public int Quantity { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int? CategoryID { get; set; }
        public virtual Category Category { get; set; }

    }
}
