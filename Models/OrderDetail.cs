﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreManagementSystem.Models
{
    public class OrderDetail
    {
        public int OrderDetailId { get; set; }
        
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public int ProductID { get; set; }
        public Product Product { get; set; }
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
    }
}
