﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using StoreManagementSystem.Data;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _db;
        public UserController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index(string searchString)
        {
            IEnumerable<Product> products;
            if (!String.IsNullOrEmpty(searchString))
            {
                products = _db.Products.Where(s => s.ProductName.Contains(searchString));
            }
            else
            {
                products = _db.Products;
            }
                /*IEnumerable<Product> products = from m in _db.Products
                             select m;
                if (!String.IsNullOrEmpty(searchString))
                {
                    products = products.Where(s => s.ProductName.Contains(searchString));
                }*/
                /*IEnumerable<Product> products = _db.Products;*/
                return View(products);
        }
        public IActionResult CategoryDropdown()
        {
            List<Category> cat = new List<Category>();
            cat = _db.Categories.ToList();
            ViewBag.message = cat;
            return View(cat);
        }
        
    }
}