﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StoreManagementSystem.Data;
using StoreManagementSystem.Models;
using StoreManagementSystem.ViewModels;

namespace StoreManagementSystem.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly ApplicationDbContext _db;
        public ShoppingCartController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AddToCart(int id)
        {
            //retrieve the product from the database
            var addedProduct = _db.Products.Single(product => product.Id == id);
            var cart = new ShoppingCart(_db, HttpContext);
            cart.AddToCart(addedProduct);

            //go back to the main store page for more shopping
            return RedirectToAction("Index", "User");
        }
        public IActionResult DisplayCart()
        {
            var cart = new ShoppingCart(_db, HttpContext);
            //Set up our ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal()
            };
            return View(viewModel);
        }
    }
}