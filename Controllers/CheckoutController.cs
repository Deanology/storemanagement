﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using StoreManagementSystem.Data;

namespace StoreManagementSystem.Controllers
{
    public class CheckoutController : Controller
    {
        private readonly UserManager<MyIdentityUser> userManager;
        public CheckoutController(UserManager<MyIdentityUser> userManager)
        {
            this.userManager = userManager;
        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            //MyIdentityUser user = userManager.GetUserAsync(HttpContext.User).Result;
            //ViewBag.Message = $"Welcome {user.Email}";
            //if(userManager.IsInRoleAsync(user, "Customer").Result)
            //{
            //    ViewBag.RoleMessage = "You are a Customer User";
            //}
            return View();
        }
    }
}