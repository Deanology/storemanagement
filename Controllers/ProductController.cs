﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StoreManagementSystem.Data;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class ProductController : Controller
    {
        private readonly ApplicationDbContext _db;
        public ProductController(ApplicationDbContext db)
        {
            _db = db;
        }
        [Authorize(Roles = "Admin")]
        public IActionResult AllProducts()
        {
            IEnumerable<Product> products = _db.Products;
            return View(products);
        }
        public IActionResult SingleProduct(int id)
        {
            var singleProduct = _db.Products.Find(id);
            return View(singleProduct);
        }
        //GET
        [Authorize(Roles = "Admin")]
        public IActionResult AddProduct()
        {
            return View();
        }
        //POST
        [HttpPost]
        public IActionResult AddProduct(Product obj)
        {
            if (ModelState.IsValid)
            {
                _db.Products.Add(obj);
                _db.SaveChanges();
                return RedirectToAction("AllProducts", "Product");
            }
            return View(obj);
        }
        //GET 
        [Authorize(Roles = "Admin")]
        public IActionResult EditProduct(int? id)
        {
            if (id == null || id == 0)
                return NotFound();
            var obj = _db.Products.Find(id);
            if (obj == null)
                return NotFound();
            return View(obj);
        }
        //POST
        [HttpPost]
        public IActionResult EditProduct(Product obj)
        {
            if (ModelState.IsValid)
            {
                _db.Products.Update(obj);
                _db.SaveChanges();
                return RedirectToAction("AllProducts", "Product");
            }
            return View(obj);
        }
        //GET 
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteProduct(int? id)
        {
            if (id == null || id == 0)
                return NotFound();
            var obj = _db.Products.Find(id);
            if (obj == null)
                return NotFound();
            return View(obj);
        }
        //POST
        [HttpPost]
        public IActionResult DeleteConfirmed(int? id)
        {
            var obj = _db.Products.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            _db.Products.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("AllProducts", "Product");
        }
    }
}