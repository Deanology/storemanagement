﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreManagementSystem.Data;
using StoreManagementSystem.Models;

namespace StoreManagementSystem.Controllers
{
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _db;
        public AdminController(ApplicationDbContext db)
        {
            _db = db;
        }
        [Authorize(Roles = "Admin")]
        public IActionResult AllCategories()
        {
            IEnumerable<Category> categories = _db.Categories;
            return View(categories);
        }
        //GET 
        [Authorize(Roles = "Admin")]
        public IActionResult CreateCategory()
        {
            return View();
        }
        //POST

        [HttpPost]
        
        public IActionResult CreateCategory(Category obj)
        {
            if (ModelState.IsValid)
            {
                _db.Categories.Add(obj);
                _db.SaveChanges();
                return RedirectToAction("AllCategories", "Admin");
            }
            return View(obj);
        }
        //GET 
        [Authorize(Roles = "Admin")]
        public IActionResult EditCategory(int? id)
        {
            if (id == null || id == 0)
                return NotFound();
            var obj = _db.Categories.Find(id);
            if (obj == null)
                return NotFound();
            return View(obj);
        }
        //POST
        [HttpPost]
        public IActionResult EditCategory(Category obj)
        {
            if (ModelState.IsValid)
            {
                _db.Categories.Update(obj);
                _db.SaveChanges();
                return RedirectToAction("AllCategories", "Admin");
            }
            return View(obj);
        }

        //GET 
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteCategory(int? id)
        {
            if (id == null || id == 0)
                return NotFound();
            var obj = _db.Categories.Find(id);
            if (obj == null)
                return NotFound();
            return View(obj);
        }
        //POST
        [HttpPost]
        public IActionResult DeleteConfirmed(int? id)
        {
            var obj = _db.Categories.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            _db.Categories.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("AllCategories", "Admin");
        }
    }
}