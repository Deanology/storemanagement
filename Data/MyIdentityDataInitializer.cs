﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreManagementSystem.Data
{
    public class MyIdentityDataInitializer
    {
        public static void SeedData(UserManager<MyIdentityUser> userManager, RoleManager<MyIdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }
        public static void SeedUsers(UserManager<MyIdentityUser> userManager)
        {
            if(userManager.FindByNameAsync("JohnDoe").Result == null)
            {
                MyIdentityUser user = new MyIdentityUser();
                user.UserName = "JohnDoe";
                user.Email = "johndoe@gmail.com";

                IdentityResult result = userManager.CreateAsync(user, "ZXcvbn55!").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Customer").Wait();
                }
            }
            if (userManager.FindByNameAsync("IdamSamuel").Result == null)
            {
                MyIdentityUser user = new MyIdentityUser();
                user.UserName = "IdamSamuel";
                user.Email = "okechukwuidam516@gmail.com";

                IdentityResult result = userManager.CreateAsync(user, "QWerty55!").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }
        }
        public static void SeedRoles(RoleManager<MyIdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("Customer").Result)
            {
                MyIdentityRole role = new MyIdentityRole();
                role.Name = "Customer";
                //role.Description = "Perform specific operations";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                MyIdentityRole role = new MyIdentityRole();
                role.Name = "Admin";
                //role.Description = "Perform all operations";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }
        }
    }
}
